<?php

return array(
    'credentials' => array(
        'key' => env('OMNI_USERNAME', ''),
        'secret' => env('OMNI_PASSWORD', '')
    )
);
