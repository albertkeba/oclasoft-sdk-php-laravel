<?php

namespace Omni\Laravel;

use Illuminate\Support\Facades\Facade;

class OmniFacade extends Facade
{
    protected static function getFacadeAccessor() {
        return 'omni';
    }
}
