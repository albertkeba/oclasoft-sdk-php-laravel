<?php

namespace Omni\Laravel;

use Omni\Rest\Client;
use Illuminate\Support\ServiceProvider;

class OmniServiceProvider extends ServiceProvider
{
    public function boot() {
        $this->mergeConfigFrom(
            __DIR__ . '/config/omni_default.php',
            'omni'
        );

        $this->publishes(array(
            __DIR__ . '/config/omni_default.php' => config_path('omni.php')
        ), 'config');
    }

    public function register() {


        // $this->app->singleton('omni', function($app) {
        //     $config = $app->make('config')->get('omni');
        //
        //     return new Client('albert.keba@kwambalabs.ga', 'Eclipse02O', 'http://192.162.69.77/api/');
        // });
        //
        // $this->app->alias('omni', 'Omni\Rest\Client');
    }
}
